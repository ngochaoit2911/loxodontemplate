﻿/*
 * MIT License
 *
 * Copyright (c) 2018 Clark Yang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of 
 * this software and associated documentation files (the "Software"), to deal in 
 * the Software without restriction, including without limitation the rights to 
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
 * of the Software, and to permit persons to whom the Software is furnished to do so, 
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all 
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
 * SOFTWARE.
 */

using Loxodon.Framework.Asynchronous;
using Loxodon.Framework.Execution;
using Loxodon.Framework.Observables;
using Loxodon.Framework.Prefs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using UnityEngine;
using UnityEngine.Events;
using XLua;
using IAsyncResult = Loxodon.Framework.Asynchronous.IAsyncResult;

namespace Loxodon.Framework.Editors
{
    public static class LuaFrameworkGenConfig
    {
        [LuaCallCSharp]
        public static List<Type> lua_call_cs_list = new List<Type>()
        {
            typeof(Executors),
            typeof(Preferences),
            typeof(AsyncResult),
            typeof(ObservableDictionary<object,object>),
            typeof(ObservableList<object>),
            typeof(NotifyCollectionChangedEventArgs),
            typeof(NotifyCollectionChangedEventHandler),
            typeof(Type),
            typeof(CoroutineAwaiterExtensions),
            // typeof(UnityAction<bool>),
            // typeof(UnityAction<object>),
            // typeof(UnityAction),
        };

        [CSharpCallLua]
        public static List<Type> cs_call_lua_list = new List<Type>()
        {
            typeof(IEnumerator),
            typeof(Action),
            typeof(Action<LuaTable>),
            typeof(Action<MonoBehaviour>),
            typeof(Func<MonoBehaviour, ILuaTask>),
            typeof(NotifyCollectionChangedEventHandler),
            typeof(Action<float>),
            typeof(Action<int>),
            typeof(Action<string>),
            typeof(Action<object>),
            typeof(Action<Exception>),
            typeof(Action<IAsyncResult>),
            typeof(EventHandler),
            typeof(Func<object>),
            typeof(UnityAction),
            typeof(UnityAction<object>),
            typeof(IAwaiter),
            typeof(IAwaiter<object>),
            typeof(IAwaiter<int>),
            typeof(ILuaTask<int>)
        };

        [BlackList]
        public static List<List<string>> BlackList = new List<List<string>>()  {
               new List<string>(){"System.Type", "IsSZArray"}
        };
        [LuaCallCSharp]
        [ReflectionUse]
        public static List<Type> dotween_lua_call_cs_list = new List<Type>()
        {
            typeof(DG.Tweening.AutoPlay),
            typeof(DG.Tweening.AxisConstraint),
            typeof(DG.Tweening.Ease),
            typeof(DG.Tweening.LogBehaviour),
            typeof(DG.Tweening.LoopType),
            typeof(DG.Tweening.PathMode),
            typeof(DG.Tweening.PathType),
            typeof(DG.Tweening.RotateMode),
            typeof(DG.Tweening.ScrambleMode),
            typeof(DG.Tweening.TweenType),
            typeof(DG.Tweening.UpdateType),

            typeof(DG.Tweening.DOTween),
            typeof(DG.Tweening.DOVirtual),
            typeof(DG.Tweening.EaseFactory),
            typeof(DG.Tweening.Tweener),
            typeof(DG.Tweening.Tween),
            typeof(DG.Tweening.Sequence),
            typeof(DG.Tweening.TweenParams),
            typeof(DG.Tweening.Core.ABSSequentiable),

            typeof(DG.Tweening.Core.TweenerCore<Vector3, Vector3, DG.Tweening.Plugins.Options.VectorOptions>),

            typeof(DG.Tweening.TweenCallback),
            typeof(DG.Tweening.TweenExtensions),
            typeof(DG.Tweening.TweenSettingsExtensions),
            typeof(DG.Tweening.ShortcutExtensions),
            typeof(TMPro.TMP_InputField),
            typeof(TMPro.TextMeshProUGUI),

            //dotween pro 的功能
            //typeof(DG.Tweening.DOTweenPath),
            //typeof(DG.Tweening.DOTweenVisualManager),
        };
    }
}